﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace CrudOpSql
{
    public partial class UserControl2 : UserControl
    {
        public UserControl2()
        {
            InitializeComponent();
        }
        private void Addgrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Course", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Course values (@CourseName, @Code)", con);
                cmd.Parameters.AddWithValue("@CourseName", textBox1.Text);
                cmd.Parameters.AddWithValue("@Code", textBox2.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                Addgrid();
            }
            catch
            {
                MessageBox.Show("Record is not right ");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Addgrid();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox3.Text != ""  && comboBox1.Text!="")
            {
                try
                {

                    if (comboBox1.Text=="Code") 
                    {

                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("Update Course Set @Name=Name  WHERE @Code=Code", con);
                        cmd.Parameters.AddWithValue("@Name", textBox1.Text);
                        cmd.Parameters.AddWithValue("@Code", textBox3.Text);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully saved");
                        Addgrid();
                    }
                }
                catch (Exception w)
                {
                    MessageBox.Show(w.Message);
                }
            }
            else
            {
                MessageBox.Show("Write code or name (On basis of combo box) of Course in searchBox ");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox3.Text != "")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Delete from Course WHERE Code=@Code", con);
                cmd.Parameters.AddWithValue("@Code", textBox3.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted..");
                Addgrid();
            }
            else
            {
                MessageBox.Show("Enetr Code no in Search Box !");
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Code")
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select * from Course where @Code = Code", con);
                cmd.Parameters.AddWithValue("@Code", textBox3.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

            }
            else if (comboBox1.Text == "Course Name")
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select * from Course where @Name= Name", con);
                cmd.Parameters.AddWithValue("@Name", textBox3.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
