﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrudOpSql
{
    public partial class UserControl1 : UserControl
    {
        public UserControl1()
        {
            InitializeComponent();
      
        }

        private void Addtogrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
       

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Student values (@RegistrartionNo, @Name,@Department,@Session,@Address)", con);
                cmd.Parameters.AddWithValue("@RegistrartionNo", textBox1.Text);
                cmd.Parameters.AddWithValue("@Name", textBox2.Text);
                cmd.Parameters.AddWithValue("@Department", textBox3.Text);
                cmd.Parameters.AddWithValue("@Session", int.Parse(textBox4.Text));
                cmd.Parameters.AddWithValue("@Address", textBox5.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                Addtogrid();
            }
            catch
            {
                MessageBox.Show("Record is not right ");
            }

        }

        private void UserControl1_Load(object sender, EventArgs e)
        {
            Addtogrid();

        }
          

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
                if (comboBox1.Text == "Registration No.")
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Select * from Student where @RegistrartionNo = RegistrartionNo", con);
                    cmd.Parameters.AddWithValue("@RegistrartionNo", textBox6.Text);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dataGridView1.DataSource = dt;

                }
                else if (comboBox1.Text == "Name")
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Select * from Student where @Name= Name", con);
                    cmd.Parameters.AddWithValue("@Name", textBox6.Text);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dataGridView1.DataSource = dt;

                }
                else if (comboBox1.Text == "Address")
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Select * from Student where @Address= Address", con);
                    cmd.Parameters.AddWithValue("@Address", textBox6.Text);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dataGridView1.DataSource = dt;

                }
                else if (comboBox1.Text == "Session")
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Select * from Student where @Session= Session", con);
                    cmd.Parameters.AddWithValue("@Session", textBox6.Text);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dataGridView1.DataSource = dt;

                }
                else if (comboBox1.Text == "Department")
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Select * from Student where @Department= Department", con);
                    cmd.Parameters.AddWithValue("@Department", textBox6.Text);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dataGridView1.DataSource = dt;

                }  
        }



        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Addtogrid();
        }

        private void update_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Registration No.")
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Update Student Set RegistrartionNo=@RegistrartionNo, Name=@Name, Department=@Department, Session=@Session, Address=@Address WHERE @RegistrartionNo=RegistrartionNo", con);
                    cmd.Parameters.AddWithValue("@RegistrartionNo", textBox1.Text);
                    cmd.Parameters.AddWithValue("@Name", textBox2.Text);
                    cmd.Parameters.AddWithValue("@Department", textBox3.Text);
                    cmd.Parameters.AddWithValue("@Session", int.Parse(textBox4.Text));
                    cmd.Parameters.AddWithValue("@Address", textBox5.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                    Addtogrid();
                }
                catch(Exception w)
                {
                    MessageBox.Show(w.Message);
                }

            }
            else if (comboBox1.Text == "Name")
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select * from Student where @Name= Name", con);
                cmd.Parameters.AddWithValue("@Name", textBox6.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

            }
            else if (comboBox1.Text == "Address")
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select * from Student where @Address= Address", con);
                cmd.Parameters.AddWithValue("@Address", textBox6.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

            }
            else if (comboBox1.Text == "Session")
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select * from Student where @Session= Session", con);
                cmd.Parameters.AddWithValue("@Session", textBox6.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

            }
            else if (comboBox1.Text == "Department")
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select * from Student where @Department= Department", con);
                cmd.Parameters.AddWithValue("@Department", textBox6.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox6.Text != "") 
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Delete from Student WHERE RegistrartionNo=@RegistrartionNo", con);
                cmd.Parameters.AddWithValue("@RegistrartionNo", textBox6.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted..");
                Addtogrid();
            }
            else
            {
                MessageBox.Show("Enetr Registration no in Search Box !");
            }
            
        }
    }
}
